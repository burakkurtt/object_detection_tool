import time 
import numpy as np
import argparse
from PIL import Image
import tensorflow as tf
import matplotlib.pyplot as plt
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils

parser = argparse.ArgumentParser(description="Saved Model Testing")
parser.add_argument("--saved_model_dir",
                    help="Saved model directory.",
                    type=str)
parser.add_argument("--label_dir",
                    help="Model label directory.",
                    type=str)
parser.add_argument("--image_dir",
                    help="Test image directory.",
                    type=str)
parser.add_argument("--image_name",
                    help="Test image name.",
                    type=str)

args = parser.parse_args()

print('Loading model ...')
start_time = time.time()

# Load saved model and build the detection function
detect_fcn = tf.saved_model.load(args.saved_model_dir)

end_time = time.time()
elapsed_time = end_time - start_time

print('Done! Took {} seconds' .format(elapsed_time))

category_index = label_map_util.create_category_index_from_labelmap(args.label_dir, \
                                                                    use_display_name=True)

image_path = args.image_dir + args.image_name

print('Running inference for {}... '.format(image_path), end='')

image_np = np.array(Image.open(image_path))
plt.imshow(image_np)
plt.savefig(args.image_dir + 'detected_' + args.image_name)
# Things to try:
# Flip horizontally
# image_np = np.fliplr(image_np).copy()

# Convert image to grayscale
# image_np = np.tile(
#     np.mean(image_np, 2, keepdims=True), (1, 1, 3)).astype(np.uint8)

# The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
input_tensor = tf.convert_to_tensor(image_np)
# The model expects a batch of images, so add an axis with `tf.newaxis`.
input_tensor = input_tensor[tf.newaxis, ...]

input_tensor = np.expand_dims(image_np, 0)

detections = detect_fcn(input_tensor)

# All outputs are batches tensors.
# Convert to numpy arrays, and take index [0] to remove the batch dimension.
# We're only interested in the first num_detections.
num_detections = int(detections.pop('num_detections'))
detections = {key: value[0, :num_detections].numpy()
                for key, value in detections.items()}
detections['num_detections'] = num_detections

# detection_classes should be ints.
detections['detection_classes'] = detections['detection_classes'].astype(np.int64)

image_np_with_detections = image_np.copy()

viz_utils.visualize_boxes_and_labels_on_image_array(image_np_with_detections,
                                                    detections['detection_boxes'],
                                                    detections['detection_classes'],
                                                    detections['detection_scores'],
                                                    category_index,
                                                    use_normalized_coordinates=True,
                                                    max_boxes_to_draw=100,
                                                    min_score_thresh=.80,
                                                    agnostic_mode=False)

plt.figure()
plt.imshow(image_np_with_detections)
plt.savefig(args.image_dir + 'detected_' + args.image_name)
print('Done')