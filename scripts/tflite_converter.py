import tensorflow as tf 
import argparse

parser = argparse.ArgumentParser(description="Tflite converting")
parser.add_argument("--saved_model_dir",
                    help="Saved model directory.",
                    type=str)
parser.add_argument("--tflite_saving_dir",
                    help="Saved model directory.",
                    type=str)

args = parser.parse_args()

# Convert the model
converter =  tf.lite.TFLiteConverter.from_saved_model(args.saved_model_dir)
tflite_model = converter.convert()

# Save the model.
with open(args.tflite_saving_dir + 'my_model.tflite', 'wb') as f:
  f.write(tflite_model)
